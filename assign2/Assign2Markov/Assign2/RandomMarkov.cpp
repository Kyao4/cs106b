/*
 * Program:RandomMarkov.cpp 
 * This program generates sensible text 
 * based on the the file input.
 *
 */


#include<iostream>
#include<fstream>
#include"strutils.h"
#include"map.h"
#include"vector.h"
#include"simpio.h"
#include"queue.h"
#include"Random.h"


/*
 *
 * Function: VectorCharatString
 * Usage: VectorCharatString(Vector<char>, start_index, end_index)
 * -------------------------------
 */
string VectorChartoString(Vector<char> window, int start, int end)
{
		char arr_seed[10];
		for(int i = start; i < end + 1; i++)
		{
			arr_seed[i] = window[i];
		}
		
		string str_seed = string(arr_seed, start, end);
		return str_seed;
}


/*
 * Function: SetFilename
 * Usage: SetFilename(ifstream)
 * -----------------------------
 */
void SetFilename(ifstream &in)
{
	cout << "Please enter filename containing source text:" ;
	while(true)
	{
		string filename = GetLine();
		in.open(filename.c_str());
		if(!in.fail()) 	return;
		in.clear();
		cout << "Cannot open file named \"" << filename 
			<< "\". Please try again." << endl;
	}
	
}

/*
 * Function: SetOrder
 * Usage: SetOrder(ifstream)
 * -----------------------------
 * parameter passed by reference.
 */
void SetOrder(int &order)
{
	while(true)
	{
		cout << "What order of analysis? (a number from 1 to 10):";
		order = GetInteger();
		if(order >= 1 && order <= 10) break; 
		cout << "Try again." << endl;
	}
	cout << "Analyzing... Please wait." << endl;
}


/* Callback function */
void OutputEach(string key, Vector<char> predictVector);
/*
 * Function: ReadFile_Map
 * Usage: ReadFile_Map(ifstream, Map, order)
 * -----------------------------
 * Collect the frequency of certain seed and following seeds then store them into a map.
 * Data structure: nested Map. Put Vector of following seeds into the Map.
 * Key of the map is the seed.
 * order: the order of the analysis.(which means how many chars to read)
 */
void ReadFile_Map(ifstream &in, Map<Vector<char> > &m, int &order)
{

	Vector<char> window;
	for(int i = 0; i < order + 1; i++)
	{
		char seed = char(in.get());
		if(in.fail()) break;
		//if(seed == '\n') continue;
		window.add(seed);
	}

	while(true)
	{		
		string str_seed = VectorChartoString(window, 0, window.size() - 1);

		char followSeed = window.getAt(window.size() - 1);
		
		if(m.containsKey(str_seed))
		{
			Vector<char> predictVector = m.getValue(str_seed);
			predictVector.add(followSeed);
			m.add(str_seed, predictVector);
		} else {
			Vector<char> predictVector;
			predictVector.add(followSeed);
			m.add(str_seed, predictVector);
		}
		
		window.removeAt(0);
		/*char seed = char(in.get());
		if(in.fail()) break;
		*/
		
		char seed;
		while(true)
		{
			seed = char(in.get());
			if(in.fail()) break;
			/*if(seed != '\n') break;*/
		}
		if(in.fail()) break;
		window.add(seed);
	
	}


	//m.mapAll(OutputEach);
	


}

/*
 * Function: OutputEach
 * Usage: OutputEach(key, predictList)
 * -----------------------------
 * Callback function
 */
void OutputEach(string key, Vector<char> predictVector)
{
	cout << key << "=";
	for(int i = 0; i < predictVector.size(); i++)
	{
		cout << predictVector[i] << ",";
	}
	cout << endl;
}

/*
 * Function: GetLongestSeed
 * Usage: GetLongestSeed(Map,longestSeed)
 * -----------------------------
 * Get the longest seed in a map.
 */
void GetLongestSeed(Map<Vector<char> > &m, string &longestSeed)
{
	int maxLength = 0;
	
	Map<Vector<char> >::Iterator itr = m.iterator();
	while(itr.hasNext())
	{
		string key = itr.next();
		Vector<char> predictVector = m.getValue(key);
		if(predictVector.size() > maxLength) {
			maxLength = predictVector.size();
			longestSeed = key;
		}
	}
	
}

/*
 * Function: Output
 * Usage: Output(Map, longestSeed, length, order)
 * -----------------------------
 * Output length character.
 */
void Output(Map<Vector<char> > &m, string &longestSeed, int length, int order)
{
	Vector<char> window;
	
	//产生第一个窗口
	for(int j = 0; j < longestSeed.size(); j++)
	{
		window.add(longestSeed[j]);
	}
	Vector<char> predictList = m.getValue(longestSeed);
	char nextSeed = predictList.getAt(RandomInteger(0,predictList.size() - 1));
	window.add(nextSeed);
	for(int count = 0; count < length; count ++)
	{
		//找出下个预测字符
		string key = VectorChartoString(window, 1, window.size() - 1);
	
		if(m.containsKey(key))
		{
			predictList = m.getValue(key);
		} else {
			break;
		}
		
		
		cout << window[0];
		window.removeAt(0);
		window.add(predictList.getAt(RandomInteger(0, predictList.size() - 1)));
	}

}

int main()
{
	Randomize();
	ifstream in;
	ofstream out;
	int order = 0;
	string longestSeed;
	Map<Vector<char> > predictMap;
	SetFilename(in);
	SetOrder(order);
	ReadFile_Map(in, predictMap, order);
	GetLongestSeed(predictMap, longestSeed);
	int length = 2000;
	Output(predictMap, longestSeed, length, order);
	return 0;
}