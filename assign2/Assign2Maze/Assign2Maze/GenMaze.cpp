/*
 * Project: Assign2Maze
 * Created by CS106 C++ Assignment Wizard 0.1
 *
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * [TODO: Describe assignment]
 */

#include<iostream>
#include"maze.h"
#include"Random.h"
#include"Vector.h"
#include"Grid.h"
#include"Stack.h"
#include"Queue.h"
#include"Set.h"


/*
 *	Function:DrawMaze
 *	Usage:DrawMaze(int row, int col)
 *	--------------------
 *	Draw maze with specified demension on screen.
 */
Maze DrawMaze(int row, int col)
{
	Maze m(row, col, true);
	m.draw();
	return m;
}

/*
 *	Function:SelectStart
 *	Usage:SelectStart(Maze &m)
 *	--------------------
 *	Select start point randomly.
 */
pointT SelectStart(Maze &m)
{
	int start_row = RandomInteger(0, m.numRows() - 1);
	int start_col = RandomInteger(0, m.numCols() - 1);
	pointT current_point = {start_row, start_col};
	return current_point;
}

/*
 *	Function:get_excluded
 *	Usage:get_excluded(Grid<bool>)
 *	--------------------
 *	If the grid contains one excluded point, then return it.
 */
pointT get_excluded(Grid<bool> &grid)
{
	for(int i = 0; i < grid.numRows(); i++)
	{
		for(int j = 0; j < grid.numCols(); j++)
		{
			if(!grid.getAt(i, j))
			{
				pointT p = {i, j};
				return p;
			}
		}
	}	
}

/*
 *	Function:GenNextPointList
 *	Usage:GenNextPointList(Maze, pointT, Grid<bool>)
 *	--------------------
 *	Generate next points list.
 */
Vector<pointT> GenNextPointList(Maze &m, pointT &current_point, Grid<bool> &grid)
{
	int current_row = current_point.row;
	int current_col = current_point.col;
	
	Vector<pointT> nextPointList;
	//left
	if(current_col - 1 >= 0)
	{
		pointT nextPoint = {current_row, current_col - 1};
		nextPointList.add(nextPoint);
	}
	//top
	if(current_row + 1 < m.numRows())
	{
		pointT nextPoint = {current_row + 1, current_col};
		nextPointList.add(nextPoint);
	}
	//right
	if(current_col + 1 < m.numCols())
	{
		pointT nextPoint = {current_row, current_col + 1};
		nextPointList.add(nextPoint);
	}
	//bottom
	if(current_row - 1 >= 0)
	{
		pointT nextPoint = {current_row - 1, current_col};
		nextPointList.add(nextPoint);
	}


	return nextPointList;
}


/*
 *	Function:IncludePoint
 *	Usage:IncludePoint(Grid<bool>, pointT)
 *	--------------------
 *	Set one point to included.
 */
void IncludePoint(Grid<bool> &grid, pointT &p)
{
	grid.setAt(p.row, p.col, true);
}

/*
 *	Function:contain_excluded
 *	Usage:contain_excluded(Grid<bool>)
 *	--------------------
 *	If grid contains excluded point, return true.
 */
void initGrid(Grid<bool> &grid)
{
	for(int i = 0; i < grid.numRows(); i ++)
	{
		for(int j = 0; j < grid.numCols(); j++)
		{
			grid.setAt(i, j, false);
		}
	}
}

/*
 *	Function:contain_excluded
 *	Usage:contain_excluded(Grid<bool>)
 *	--------------------
 *	If grid contains excluded point, return true.
 */
bool contain_excluded(Grid<bool> &grid)
{
	for(int i = 0; i < grid.numRows(); i ++)
	{
		for(int j = 0; j < grid.numCols(); j++)
		{
			if(!grid.getAt(i, j)) return true;
		}
	}	
	return false;
}

void DisplayGrid(Grid<bool> &g)
{
	for(int i = 0; i < g.numRows(); i++)
	{
		for(int j = 0; j < g.numCols(); j++)
		{
			cout << g.getAt(i, j);
			
		}
		cout << endl;
	}
}

bool IsExcluded(Grid<bool> &g, pointT &p)
{
	return !g.getAt(p.row, p.col);
}

/*
 *	Function:GenMaze
 *	Usage:GenMaze(Maze)
 *	--------------------
 *	Draw the whole maze.
 *
 */
void GenMaze(Maze &m, Grid<bool> &grid)
{

	

	pointT current_point = SelectStart(m);
	while(true)
	{
		//DisplayGrid(grid);
	 	if(!contain_excluded(grid))
	 	{
	 		break;
	 	}
		IncludePoint(grid, current_point);
		Vector<pointT> nextPointList = GenNextPointList(m, current_point, grid);
		pointT next_point = nextPointList.getAt(RandomInteger(0, nextPointList.size() - 1));
		

		if(IsExcluded(grid, next_point))
		{
			m.setWall(current_point, next_point, false);
		}
		current_point = next_point;
	}
}

bool check_end(Maze &m, pointT &p)
{
	int end_row = m.numRows() - 1;
	int end_col = m.numCols() - 1;
	if( p.row == end_row && p.col == end_col)
	{
		return true;
	}
	return false;
}

Vector<pointT> choose_next_path_point(Maze &m, Vector<pointT> &nextPointList, pointT &current_point, Set<pointT> &set)
{
	Vector<pointT> next_path_points;
	for(int i = 0; i < nextPointList.size(); i++)
	{

		pointT next_path_point = nextPointList.getAt(i);
		
		
		if(!m.isWall(next_path_point, current_point) && !set.contains(next_path_point))
		{
			next_path_points.add(next_path_point);
			set.add(next_path_point);
		}
	}
	return next_path_points;
}

int compare_pointT(pointT p1, pointT p2)
{
		if (p1.row == p2.row && p1.col == p2.col) return 0;
		else if (p1.row < p2.row && p1.col < p2.col) return -1;
		else return 1;
}

void SolveMaze(Maze &m, Grid<bool> &grid)
{
	
	Queue<Stack<pointT> > path_buffer;
	Stack<pointT> current_path;
	//初始化集合
	Set<pointT> set(compare_pointT);
	
	pointT current_point = {0, 0};
	set.add(current_point);
	m.drawMark(current_point, "blue");

	
	current_path.push(current_point);
	path_buffer.enqueue(current_path);
	Stack<pointT> solution;

	while(true)
	{
		
		current_path = path_buffer.dequeue();
		current_point = current_path.peek();
		
		//当前点加入集合
		set.add(current_point);
		//m.drawMark(current_point, "blue");
		if(check_end(m, current_point))
		{
			solution = current_path;
			break;
		}

		Vector<pointT> nextPointList = GenNextPointList(m, current_point, grid);
		//防止回溯


		//Set<pointT>::Iterator itr = set.iterator();
		//while(itr.hasNext())
		//{
		//	pointT p = itr.next();
		//	cout << p.row << p.col << endl;
		//}
		
		Vector<pointT> next_path_point_list = choose_next_path_point(m, nextPointList, current_point, set);
		Stack<pointT> next_path;


		//输出当前的路径
		//Stack<pointT> new_path = current_path;
		//while (!new_path.isEmpty())
		//{
		//	pointT  p = new_path.pop();
		//	
		//	cout << p.row << "," << p.col << endl;
		//	
		//}
		
		for(int i = 0; i < next_path_point_list.size(); i++)
		{
			next_path = current_path;
			pointT next_point = next_path_point_list.getAt(i);
			next_path.push(next_point);

			path_buffer.enqueue(next_path);
		}

	}

	while (!solution.isEmpty())
	{
		m.drawMark(solution.pop(), "black");
	}





	

	
	
}

int main ()
{
	Randomize();
	int row = 17;
	int col = 30;
	Maze m = DrawMaze(row, col);

	Grid<bool> grid(m.numRows(), m.numCols());
	initGrid(grid);
	while(true)
	{

		GenMaze(m, grid);
		cout << "Hit RETURN to solve.";
		int input = cin.get();
		if(input == '\n')
		{
			SolveMaze(m, grid);
		}
		cout << "Hit RETURN to regenerate.";
		int input_repeat = cin.get();
		if(input == '\n' )
		{
			m = DrawMaze(row, col);
			initGrid(grid);
			continue;
		} else {
			cout << "wrong input. EXIT!";
			break;
		}
	}


	return 0;
}