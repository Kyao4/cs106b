/*
 * Project: assign3RecursivePuzzle
 * Created by CS106 C++ Assignment Wizard 0.1
 *
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * [TODO: Describe assignment]
 */


#include <iostream>
#include "vector.h"


bool Solvable(int start, Vector<int> & squares);
bool Solvable_used_check(int start, Vector<int> & squares, Vector<int> & squares_used);
int main ()
{

	Vector<int> numList;
	numList.add(3);
	numList.add(6);
	numList.add(4);
	numList.add(1);
	numList.add(3);
	numList.add(4);
	numList.add(2);
	numList.add(5);
	numList.add(3);
	numList.add(0);

	
	cout << Solvable(0, numList) << endl;



	return 0;
}


bool Solvable(int start, Vector<int> & squares) {
	Vector<int> squares_used;
	for(int i = 0; i < squares.size(); i++) {
		squares_used.add(false);
	}
	return Solvable_used_check(start, squares, squares_used);
}

bool Solvable_used_check(int start, Vector<int> & squares, Vector<int> & squares_used) {

	if(start == squares.size() - 1) {
		return true;
	}

	//try left first
	int left_index = start - squares[start];
	if(left_index >= 0 && left_index <= squares.size() - 1 && !squares_used[left_index]) {
		squares_used[left_index] = true;
		if (Solvable_used_check(left_index, squares, squares_used)) {
			return true;
		}
		
	}

	//try right
	int right_index = start + squares[start];
	if(right_index >= 0 && right_index <= squares.size() - 1 && !squares_used[right_index]) {
		squares_used[right_index] = true;
		if(Solvable_used_check(right_index, squares, squares_used)) {
			return true;
		} 
	}

	return false;
}