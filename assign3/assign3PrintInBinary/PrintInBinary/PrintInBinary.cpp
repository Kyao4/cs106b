/*
 * Project: PrintInBinary
 * Created by CS106 C++ Assignment Wizard 0.1
 *
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * [TODO: Describe assignment]
 */

#include<iostream>

using namespace std;
#include"simpio.h"
void PrintInBinary(int number)
{
	int remainder = number%2;
	number /= 2;
	
	if(number == 0)
	{
		cout << remainder;
		return;
	}
	
	PrintInBinary(number);
	cout << remainder;
	
	
}



int main ()
{
	cout << "Input an integer." << endl;
	int input = GetInteger();
	PrintInBinary(input);
	return 0;
}
