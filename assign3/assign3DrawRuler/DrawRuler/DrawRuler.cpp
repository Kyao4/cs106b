/*
 * Project: DrawRuler
 * Created by CS106 C++ Assignment Wizard 0.1
 *
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * [TODO: Describe assignment]
 */

#include"Graphics.h"

const double HEIGHT = 0.5;
const double MIN_HEIGHT = HEIGHT/16;
const double WIDTH = 3.0;


void DrawRuler(double x, double y, double w, double h) {
	if(h <= MIN_HEIGHT) {
		return;
	} else {
		MovePen(x, y);
		DrawLine(w, 0); //horizontal line
		double mid = x + w / 2;
		MovePen(mid, y);
		DrawLine(0, h); //vertical line
		DrawRuler(x, y, w/2, h/2); // left part
		DrawRuler(mid, y, w/2, h/2); // right part
	}
}


int main ()
{
	InitGraphics();
	DrawRuler(1.0, 1.0, WIDTH, HEIGHT);
	return 0;
}
