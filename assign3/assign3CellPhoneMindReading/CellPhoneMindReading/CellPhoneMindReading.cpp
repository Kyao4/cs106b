/*
 * Project: CellPhoneMindReading
 * Created by CS106 C++ Assignment Wizard 0.1
 *
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * [TODO: Describe assignment]
 */
#include"lexicon.h"
#include<iostream>
#include"simpio.h"


void IsWord(string word, Lexicon & lex) { 
	if(lex.containsWord(word)) {
		cout << word << endl;
		IsWord(word + "a", lex);
		IsWord(word + "b", lex);
		IsWord(word + "c", lex);
		IsWord(word + "d", lex);
		IsWord(word + "e", lex);
		IsWord(word + "f", lex);
		IsWord(word + "g", lex);
		IsWord(word + "h", lex);
		IsWord(word + "i", lex);
		IsWord(word + "g", lex);
		IsWord(word + "k", lex);
		IsWord(word + "l", lex);
		IsWord(word + "m", lex);
		IsWord(word + "n", lex);
		IsWord(word + "o", lex);
		IsWord(word + "p", lex);
		IsWord(word + "q", lex);
		IsWord(word + "r", lex);
		IsWord(word + "s", lex);
		IsWord(word + "t", lex);
		IsWord(word + "u", lex);
		IsWord(word + "v", lex);
		IsWord(word + "w", lex);
		IsWord(word + "x", lex);
		IsWord(word + "y", lex);
		IsWord(word + "z", lex);
	} else if(lex.containsPrefix(word)){
		IsWord(word + "a", lex);
		IsWord(word + "b", lex);
		IsWord(word + "c", lex);
		IsWord(word + "d", lex);
		IsWord(word + "e", lex);
		IsWord(word + "f", lex);
		IsWord(word + "g", lex);
		IsWord(word + "h", lex);
		IsWord(word + "i", lex);
		IsWord(word + "g", lex);
		IsWord(word + "k", lex);
		IsWord(word + "l", lex);
		IsWord(word + "m", lex);
		IsWord(word + "n", lex);
		IsWord(word + "o", lex);
		IsWord(word + "p", lex);
		IsWord(word + "q", lex);
		IsWord(word + "r", lex);
		IsWord(word + "s", lex);
		IsWord(word + "t", lex);
		IsWord(word + "u", lex);
		IsWord(word + "v", lex);
		IsWord(word + "w", lex);
		IsWord(word + "x", lex);
		IsWord(word + "y", lex);
		IsWord(word + "z", lex);
	} 
	
}


void ListPrefixs(string prefix, string digits, Lexicon & lex) {
	if(digits.size() == 0){

		//cout << prefix << endl;
		IsWord(prefix, lex);
	} else {
		string num = digits.substr(0,1);
		switch(num[0]) {
				case '2':
			{
				ListPrefixs(prefix + "a", digits.substr(1), lex);
				ListPrefixs(prefix + "b", digits.substr(1), lex);
				ListPrefixs(prefix + "c", digits.substr(1), lex);
				break;
			}
			case '3':
			{
				ListPrefixs(prefix + "d", digits.substr(1), lex);
				ListPrefixs(prefix + "e", digits.substr(1), lex);
				ListPrefixs(prefix + "f", digits.substr(1), lex);
				break;
			}
			case '4':
			{
				ListPrefixs(prefix + "g", digits.substr(1), lex);
				ListPrefixs(prefix + "h", digits.substr(1), lex);
				ListPrefixs(prefix + "i", digits.substr(1), lex);
				break;
			}
			case '5':
			{
				ListPrefixs(prefix + "j", digits.substr(1), lex);
				ListPrefixs(prefix + "k", digits.substr(1), lex);
				ListPrefixs(prefix + "l", digits.substr(1), lex);
				break;
			}
			case '6':
			{
				ListPrefixs(prefix + "m", digits.substr(1), lex);
				ListPrefixs(prefix + "n", digits.substr(1), lex);
				ListPrefixs(prefix + "o", digits.substr(1), lex);
				break;
			}
			case '7':
			{
				ListPrefixs(prefix + "p", digits.substr(1), lex);
				ListPrefixs(prefix + "q", digits.substr(1), lex);
				ListPrefixs(prefix + "r", digits.substr(1), lex);
				ListPrefixs(prefix + "s", digits.substr(1), lex);
				break;
			}
			case '8':
			{
				ListPrefixs(prefix + "t", digits.substr(1), lex);
				ListPrefixs(prefix + "u", digits.substr(1), lex);
				ListPrefixs(prefix + "v", digits.substr(1), lex);
				break;
			}
			case '9':
			{
				ListPrefixs(prefix + "w", digits.substr(1), lex);
				ListPrefixs(prefix + "x", digits.substr(1), lex);
				ListPrefixs(prefix + "y", digits.substr(1), lex);
				ListPrefixs(prefix + "z", digits.substr(1), lex);
				break;
			}
		}
		
	}
}


void ListCompletions(string digits, Lexicon & lex) {
	string prefix = "";
	ListPrefixs(prefix,digits,lex);
}


int main ()
{
	Lexicon lex("lexicon.dat");
	string digits;
	while(true) {
	cout << "Enter the number." << endl;
	digits = GetLine();
	ListCompletions(digits,lex);
	}
}
