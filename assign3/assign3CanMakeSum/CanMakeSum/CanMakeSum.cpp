/*
 * Project: CanMakeSum
 * Created by CS106 C++ Assignment Wizard 0.1
 *
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * [TODO: Describe assignment]
 */


#include<iostream>
#include"simpio.h"
#include"Vector.h"

using namespace std;


bool IfMakeSum(int prefix, Vector<int> &list, int targetSum)
{
	if(list.size() == 0 ){
		return false;
	}
	prefix = list[0];
	list.removeAt(0);
	for(int i = 0; i < list.size(); i++) {
		if(prefix + list[i] == targetSum) return true;
	}

	return IfMakeSum(prefix, list, targetSum);
	
}

int CanMakeSum(Vector<int> &list, int targetSum)
{
	int prefix = 0;
	return IfMakeSum(prefix, list, targetSum);
}




int main ()
{
	while(true) {
	Vector<int> list;
	list.add(3);list.add(7);list.add(1);list.add(8);list.add(-3);
	
		cout << '{';
		for(int i = 0; i < list.size(); i++) {
			cout << list[i] << ',';
		}
		cout << '}' << endl;
		int targetSum = GetInteger();
		cout << CanMakeSum(list, targetSum) << endl;
	}
	return 0;
}
