/*
 * Project: 12-step
 * Created by CS106 C++ Assignment Wizard 0.1
 *
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * [TODO: Describe assignment]
 */

#include<iostream>
#include"simpio.h"

int CountWays(int numStairs) {
	if(numStairs == 1){
		return 1;
	} else if(numStairs == 2) {
		return 2;
	} else {
		return CountWays(numStairs - 1) + CountWays(numStairs - 2);
	}
	
}


int main ()
{
	int numStairs = GetInteger();
	cout << "The number of stairs is " << numStairs << endl;
	int numWays = CountWays(numStairs);
	cout << "The number of ways to climb " << numWays;
	return 0;
}
