/*
 * Project: assign3_vote_count
 * Created by CS106 C++ Assignment Wizard 0.1
 *
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * [TODO: Describe assignment]
 */

#include "genlib.h"
#include "vector.h"
#include "map.h"
#include <iostream>

int CountCriticalVotes(Vector<int> & blocks, int blockIndex); 
int CriticalVotes(Vector<int> & blocks, int currentIndex, int maxIndex, int currentVoteA, int currentVoteB, int blockVote, int totalVote);

int main ()
{
	Vector<int> blocks;
	int blockIndex = 2;
	blocks.add(4);
	blocks.add(2);
	blocks.add(7);
	blocks.add(4);
	cout << CountCriticalVotes(blocks, blockIndex) << endl; 
	return 0;
}

int CountCriticalVotes(Vector<int> & blocks, int blockIndex) {
	int maxIndex = 0;
	int currentIndex = 0;
	int currentVoteA = 0;
	int currentVoteB = 0;
	int totalVote = 0;
	
	for(int i = 0; i < blocks.size(); i++) {
		totalVote += blocks[i];
	}
	int blockVote = blocks[blockIndex];
	blocks.removeAt(blockIndex);
	maxIndex = blocks.size() - 1;
	return CriticalVotes(blocks, currentIndex, maxIndex, currentVoteA, currentVoteB, blockVote, totalVote);


}

int CriticalVotes(Vector<int> & blocks, int currentIndex, int maxIndex, int currentVoteA, int currentVoteB, int blockCount, int totalCount) {
	if (currentIndex > maxIndex && currentVoteA > currentVoteB && currentVoteA < (currentVoteB + blockCount)) {
		return 1;
	} else if (currentIndex > maxIndex && currentVoteA > currentVoteB && currentVoteA > (currentVoteB + blockCount)) {
		return 0;
	} else if (currentIndex > maxIndex && currentVoteA < currentVoteB && (currentVoteA + blockCount) > (currentVoteB)) {
		return 1;
	} else if (currentIndex > maxIndex && currentVoteA < currentVoteB && (currentVoteA + blockCount) < (currentVoteB)) {
		return 0;
	} else {
		int thisVote = blocks[currentIndex];
		currentIndex++;
		return (CriticalVotes(blocks, currentIndex, maxIndex, currentVoteA + thisVote, currentVoteB, blockCount, totalCount) + CriticalVotes(blocks, currentIndex, maxIndex, currentVoteA, currentVoteB + thisVote, blockCount, totalCount));
		
	}


	Error("error, should not reach this point.");
}
