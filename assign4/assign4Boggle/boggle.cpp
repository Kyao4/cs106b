/* File: boggle.cpp
 * ----------------
 * Your name here!
 */
 
#include "genlib.h"
#include "simpio.h"
#include <iostream>
#include <cctype>
#include "extgraph.h"
#include "grid.h"
#include "random.h"
#include "lexicon.h"
#include "gboggle.h"
#include "Set.h"


string StandardCubes[16]  = 
{"AAEEGN", "ABBJOO", "ACHOPS", "AFFKPS", "AOOTTW", "CIMOTU", "DEILRX", "DELRVY",
 "DISTTY", "EEGHNW", "EEINSU", "EHRTVW", "EIOSST", "ELRTTY", "HIMNQU", "HLNNRZ"};
 
string BigBoggleCubes[25]  = 
{"AAAFRS", "AAEEEE", "AAFIRS", "ADENNN", "AEEEEM", "AEEGMU", "AEGMNN", "AFIRSY", 
"BJKQXZ", "CCNSTW", "CEIILT", "CEILPT", "CEIPST", "DDLNOR", "DDHNOT", "DHHLOR", 
"DHLNOR", "EIIITT", "EMOTTT", "ENSSSU", "FIPRSY", "GORRVW", "HIPRRY", "NOOTUW", "OOOTTU"};
struct Point {
	int x;
	int y;
};

struct Boggle_helper {
	Grid<char> board;
	Lexicon lex;
	Set<string> humanAnswer;
	Set<string> computerAnswer;
	int humanScore;
	int computerScore;
};



void GiveInstructions()
{
    cout << endl << "The boggle board is a grid onto which I will randomly distribute " 
	 << "cubes. These 6-sided cubes have letters rather than numbers on the faces, " 
	 << "creating a grid of letters on which you try to form words. You go first, " 
	 << "entering all the words you can find that are formed by tracing adjoining " 
	 << "letters. Two letters adjoin if they are next to each other horizontally, " 
	 << "vertically, or diagonally. A letter can only be used once in the word. Words "
	 << "must be at least 4 letters long and can only be counted once. You score points "
	 << "based on word length: a 4-letter word is worth 1 point, 5-letters earn 2 "
	 << "points, and so on. After your puny brain is exhausted, I, the super computer, "
	 << "will find all the remaining words and double or triple your paltry score." << endl;
	
    cout << "\nHit return when you're ready...";
    GetLine();
}

static void Welcome()
{
    cout << "Welcome!  You're about to play an intense game of mind-numbing Boggle. " 
	 << "The good news is that you might improve your vocabulary a bit.  The "
	 << "bad news is that you're probably going to lose miserably to this little "
	 << "dictionary-toting hunk of silicon.  If only YOU had a gig of RAM..." << endl << endl;
}



void SetupBoard(Grid<char> & grid) {
	// first let user choose to have default configuration or new configuration by himself.
	while(true) {
		cout << "if you wanna use your own configuration, please enter 2, else enter 1." << endl;
		int choice = GetInteger();
		if(choice == 2) {
			cout << "Please enter your grid width. It must be less than 5." << endl;
			int width = GetInteger();
			if(width > 5) {
				Error("Dimension of the board should be less than 5.");
			}
			grid.resize(width, width);
			for(int i = 0; i < width; i++) {
				cout << "enter " << i << " row of characters." << endl;
				string line = GetLine();
				if(line.length() > width) {
					Error("Invalid string length.");
				} 
				for(int j = 0; j < width; j++) {
					grid.setAt(i, j, toupper(char(line[j])));
				}
			}
			break;
		} else if(choice == 1) { 
			grid.resize(4, 4);
			
			for(int i = 0; i < 4; i++) {
				for(int j = 0; j < 4; j++) {
					int rand_row = RandomInteger(0, 15);
					int rand_col = RandomInteger(0, 5);
					grid.setAt(i, j, char(StandardCubes[rand_row][rand_col]));
				}
			}

			break;
		}
	}
	
	

}

void DrawBoard(Grid<char> & grid) {
	DrawBoard(grid.numRows(), grid.numCols());
	for(int i = 0; i < grid.numRows(); i++) {
		for(int j = 0; j < grid.numCols(); j++) {
			LabelCube(i, j, grid.getAt(i, j));
		}
	}
}



bool isDuplicatePoint(Vector<Point>  pointList, Point currentPoint) {
	for(int i = 0; i < pointList.size(); i++) {
		if(currentPoint.x == pointList[i].x && currentPoint.y == pointList[i].y) {
			return true;
		}
	}
	return false;

}



/* 
	currentPosition: index of the character you need to test.
	currentPoint: point position on grid.
*/
bool isOnBoardBool(string word, int currentPosition, Point currentPoint, Boggle_helper & bh, Vector<Point>  pointList) {
	if(currentPosition == word.length()) {

		// mark all the point in pointList.
		for(int i = 0; i < pointList.size(); i++) {
			HighlightCube(pointList[i].x, pointList[i].y, true);
		}
		return true;
	}

	if(currentPosition == 0) {
		char initChar = toupper(char(word[currentPosition]));
		
		for(int i = 0; i < bh.board.numCols(); i++) {
			for(int j = 0; j < bh.board.numRows(); j++) {
				if(bh.board.getAt(i, j) == initChar) {
					currentPoint.x = i;
					currentPoint.y = j;
					pointList.add(currentPoint);
					if(isOnBoardBool(word, currentPosition + 1, currentPoint, bh, pointList)) return true;	
					pointList.removeAt(pointList.size() - 1);
					
				}
			}
		}
	
		return false;

	}

	//currentPosition is not 0
	
	/*
	test eight position from left bottom clockwise.
	*/
	
	Point nextPoint = {currentPoint.x + 1, currentPoint.y - 1};
	if(!isDuplicatePoint(pointList, nextPoint) && nextPoint.x >= 0 && nextPoint.x < bh.board.numCols() &&  nextPoint.y >= 0 && nextPoint.y < bh.board.numCols()) {
		pointList.add(nextPoint);
		if(bh.board.getAt(nextPoint.x, nextPoint.y) == toupper(char(word[currentPosition]))) {
			if(isOnBoardBool(word, currentPosition + 1, nextPoint, bh, pointList)) return true;
		}
		pointList.removeAt(pointList.size() - 1);
	}
	for(int i = 0; i < 2; i++) {
		nextPoint.x -= 1;
		if(!isDuplicatePoint(pointList, nextPoint) && nextPoint.x >= 0 && nextPoint.x < bh.board.numCols() &&  nextPoint.y >= 0 && nextPoint.y < bh.board.numCols()) {
			pointList.add(nextPoint);
			if(bh.board.getAt(nextPoint.x, nextPoint.y) == toupper(char(word[currentPosition]))) {
				if(isOnBoardBool(word, currentPosition + 1, nextPoint, bh, pointList)) return true;
			}
			pointList.removeAt(pointList.size() - 1);
		}
	}
	for(int i = 0; i < 2; i++) {
		nextPoint.y += 1;
		if(!isDuplicatePoint(pointList, nextPoint) && nextPoint.x >= 0 && nextPoint.x < bh.board.numCols() &&  nextPoint.y >= 0 && nextPoint.y < bh.board.numCols()) {
			pointList.add(nextPoint);
			if(bh.board.getAt(nextPoint.x, nextPoint.y) == toupper(char(word[currentPosition]))) {
				if(isOnBoardBool(word, currentPosition + 1, nextPoint, bh, pointList)) return true;
			}
			pointList.removeAt(pointList.size() - 1);
		}
	}

	for(int i = 0; i < 2; i++) {
		nextPoint.x += 1;
		if(!isDuplicatePoint(pointList, nextPoint) && nextPoint.x >= 0 && nextPoint.x < bh.board.numCols() &&  nextPoint.y >= 0 && nextPoint.y < bh.board.numCols()) {
			pointList.add(nextPoint);
			if(bh.board.getAt(nextPoint.x, nextPoint.y) == toupper(char(word[currentPosition]))) {
				if(isOnBoardBool(word, currentPosition + 1, nextPoint, bh, pointList)) return true;
			}
			pointList.removeAt(pointList.size() - 1);
		}
	}

	for(int i = 0; i < 1; i++) {
		nextPoint.y -= 1;
		if(!isDuplicatePoint(pointList, nextPoint) && nextPoint.x >= 0 && nextPoint.x < bh.board.numCols() &&  nextPoint.y >= 0 && nextPoint.y < bh.board.numCols()) {
			pointList.add(nextPoint);
			if(bh.board.getAt(nextPoint.x, nextPoint.y) == toupper(char(word[currentPosition]))) {
				if(isOnBoardBool(word, currentPosition + 1, nextPoint, bh, pointList)) return true;
			}
			pointList.removeAt(pointList.size() - 1);
		}
	}
	
	/*for(int i = 0; i < flags8.size(); i++) {
		if(flags8[i]) {
			return true;
		}
	}*/
	return false;

}




bool isOnBoard(string word,Boggle_helper & bh) {
	int currentPosition = 0;
	Point p;
	p.x = 0;
	p.y = 0;
	Vector<Point> pointList;
	return isOnBoardBool(word, currentPosition, p, bh, pointList);

}


void clearBoard(Boggle_helper & bh) {

	for(int i = 0; i < bh.board.numRows(); i++) {
		for(int j = 0; j < bh.board.numCols(); j++) {
			HighlightCube(i, j, false);
		}
	}

}


void HumanTurn(Boggle_helper & bh) {
	//read words from human.
	while(true) {
		cout << "Please enter your word: enter empty word to exit." <<endl;
		
		string word = GetLine();
		
		if(word == "") {
			cout << "exit human word input turn." << endl;
			break;
		}
		
		if(word.length() < 4) {
			cout << "Your word is too short. Please enter again." << endl;
			continue;
		}
		if(!bh.lex.containsWord(word)) {
			cout << "This is not a valid word. Please enter again." << endl;
			continue;
		}
		if(bh.humanAnswer.contains(word)) {
			cout << "You've already entered this word. Please enter again." << endl;
			continue;
		}

		clearBoard(bh);
		UpdateDisplay();

		if(!isOnBoard(word, bh)) {
			cout << "This word is not on the board. Please enter again." << endl;
			continue;
		}

		

		bh.humanAnswer.add(word);
		bh.humanScore += word.length() - 3;
		RecordWordForPlayer(word, Human);
		cout << "added word: " << word << endl;
	
	}

	
}


bool containWordsInAnswer(Vector<string> computerAnswerList, string word) {
	for(int i = 0; i < computerAnswerList.size(); i++) {
		if(computerAnswerList[i] == word) {
			return true;
		}
	}
	return false;
}

bool containWords(Point currentPoint,string & word, Boggle_helper bh, Vector<Point> & pointList, Vector<string> & computerAnswerList) {
	clearBoard(bh);
	UpdateDisplay();
	if(word.length() > 6) {
		return false;
	}
	if(bh.lex.containsWord(word) && word.length() >= 4 && !bh.humanAnswer.contains(word) && !containWordsInAnswer(computerAnswerList, word)) {
		//todo show all the blocks
		for(int i = 0; i < pointList.size(); i++) {
			HighlightCube(pointList[i].x, pointList[i].y, true);
		}
		
		UpdateDisplay();
		bh.computerAnswer.add(word);
		computerAnswerList.add(word);
		bh.computerScore += word.length() - 3;
		RecordWordForPlayer(word, Computer);
		Pause(2);
	}
	if(!bh.lex.containsPrefix(word)) { 
		return false;
	}
	
	Point nextPoint = {currentPoint.x + 1, currentPoint.y - 1};
	if(!isDuplicatePoint(pointList, nextPoint) && nextPoint.x >= 0 && nextPoint.x < bh.board.numCols() &&  nextPoint.y >= 0 && nextPoint.y < bh.board.numCols()) {
		pointList.add(nextPoint);
		word = word + char(tolower(bh.board.getAt(nextPoint.x, nextPoint.y)));
		if(containWords(nextPoint, word, bh, pointList, computerAnswerList)) return true;
		word = word.substr(0, word.size() - 1);
		pointList.removeAt(pointList.size() - 1);
	}
	for(int i = 0; i < 2; i++) {
		nextPoint.x -= 1;
		if(!isDuplicatePoint(pointList, nextPoint) && nextPoint.x >= 0 && nextPoint.x < bh.board.numCols() &&  nextPoint.y >= 0 && nextPoint.y < bh.board.numCols()) {
			pointList.add(nextPoint);
			word = word + char(tolower(bh.board.getAt(nextPoint.x, nextPoint.y)));
			if(containWords(nextPoint, word, bh, pointList, computerAnswerList)) return true;
			word = word.substr(0, word.size() - 1);
			pointList.removeAt(pointList.size() - 1);
		}
	}
	for(int i = 0; i < 2; i++) {
		nextPoint.y += 1;
		if(!isDuplicatePoint(pointList, nextPoint) && nextPoint.x >= 0 && nextPoint.x < bh.board.numCols() &&  nextPoint.y >= 0 && nextPoint.y < bh.board.numCols()) {
			pointList.add(nextPoint);
			word = word + char(tolower(bh.board.getAt(nextPoint.x, nextPoint.y)));
			if(containWords(nextPoint, word, bh, pointList, computerAnswerList)) return true;
			word = word.substr(0, word.size() - 1);
			pointList.removeAt(pointList.size() - 1);
		}
	}

	for(int i = 0; i < 2; i++) {
		nextPoint.x += 1;
		if(!isDuplicatePoint(pointList, nextPoint) && nextPoint.x >= 0 && nextPoint.x < bh.board.numCols() &&  nextPoint.y >= 0 && nextPoint.y < bh.board.numCols()) {
			pointList.add(nextPoint);
			word = word + char(tolower(bh.board.getAt(nextPoint.x, nextPoint.y)));
			if(containWords(nextPoint, word, bh, pointList, computerAnswerList)) return true;
			word = word.substr(0, word.size() - 1);
			pointList.removeAt(pointList.size() - 1);
		}
	}

	for(int i = 0; i < 1; i++) {
		nextPoint.y -= 1;
		if(!isDuplicatePoint(pointList, nextPoint) && nextPoint.x >= 0 && nextPoint.x < bh.board.numCols() &&  nextPoint.y >= 0 && nextPoint.y < bh.board.numCols()) {
			pointList.add(nextPoint);
			word = word + char(tolower(bh.board.getAt(nextPoint.x, nextPoint.y)));
			if(containWords(nextPoint, word, bh, pointList, computerAnswerList)) return true;
			word = word.substr(0, word.size() - 1);
			pointList.removeAt(pointList.size() - 1);
		}
	}
	
	/*for(int i = 0; i < flags8.size(); i++) {
		if(flags8[i]) {
			return true;
		}
	}*/
	return false;



	Error("should not reach this point.");
}

void ComputerTurn(Boggle_helper & bh) {
	cout << "computer is thinking..." << endl;
	Vector<string> computerAnswerList;
	for(int i = 0; i < bh.board.numRows(); i++) {
		for(int j = 0; j < bh.board.numCols(); j++) {
			string word = "";
			word = word + char(tolower(bh.board.getAt(i, j)));
			Point currentPoint;
			currentPoint.x = i;
			currentPoint.y = j;
			Vector<Point> pointList;
			pointList.add(currentPoint);
			clearBoard(bh);
			UpdateDisplay();
			containWords(currentPoint, word, bh, pointList, computerAnswerList);
			word = word.substr(0, word.size() - 1);
			pointList.removeAt(pointList.size() - 1);
		}
	}

}




int main()
{

	Grid<char> currentGrid;	
	Set<string> humanAnswer;
	Set<string> computerAnswer;
	Lexicon lex("lexicon.dat");
	SetWindowSize(9, 5);
	InitGraphics();
	Welcome();
	GiveInstructions();
	SetupBoard(currentGrid);
	DrawBoard(currentGrid);
	Boggle_helper bh;
	bh.board = currentGrid;
	bh.lex = lex;
	bh.humanAnswer = humanAnswer;
	bh.computerAnswer = computerAnswer;
	bh.humanScore = 0;
	bh.computerScore = 0;
	HumanTurn(bh);
	ComputerTurn(bh);
	

	return 0;
}