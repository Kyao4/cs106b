/*
 * Project: assign1
 * Created by CS106 C++ Assignment Wizard 0.1
 *
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * [TODO: Describe assignment]
 */

#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;
int RaiseIntToPower(int base, int power);
bool isPrime(int num);

int main ()
{
	for(int i = 0; i < 100; i++) {
		if(isPrime(i)) {
			int perfectNum = int(pow(2.0, i - 1) * (pow(2.0, i) - 1));
			if(perfectNum > 10000) {
				break;
			}
			cout << "Perfect number is: " << perfectNum << endl;
		}
	}
	
	return 0;
}

bool isPrime(int num) {
	
	if(num <= 1) {
		return false;
	}
	for(int i = 2; i < num; i++) {
		if(num % i == 0) {
			return false;
		}
	}
	return true;
}


int RaiseIntToPower(int base, int power) {
	int result = 1;
	for(int i = 0; i < power; i++) {
		result *= base;
	}
	return result;
}