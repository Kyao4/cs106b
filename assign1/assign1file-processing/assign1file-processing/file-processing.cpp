/*
 * Project: assign1file-processing
 * Created by CS106 C++ Assignment Wizard 0.1
 *
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * [TODO: Describe assignment]
 */
#include <iostream>
#include <fstream>
#include <vector.h>
#include <simpio.h>
#include <cctype>


void ReadTextFile(ifstream & infile, Vector<string> & lines);
void AskUserForInputFile(string prompt, ifstream & infile);



int main ()
{
	ifstream infile;
	AskUserForInputFile("Please enter name of the file.", infile);
	Vector<string> strList;
	ReadTextFile(infile, strList);
	Vector<int> resultList;
	for(int i = 0; i < 10; i++) {
		resultList.add(0);
	}

	for(int i = 0; i < strList.size(); i++) {
		switch(strList[i][0]) {
			case'0': resultList[0]++;break;
			case'1': resultList[1]++;break;
			case'2': resultList[2]++;break;
			case'3': resultList[3]++;break;
			case'4': resultList[4]++;break;
			case'5': resultList[5]++;break;
			case'6': resultList[6]++;break;
			case'7': resultList[7]++;break;
			case'8': resultList[8]++;break;
			case'9': resultList[9]++;break;
			default: Error("Wrong value.");
		}
	}
	for(int i = 0; i < resultList.size(); i++) {
		if(i == 0) {
			cout << "0-9: ";
		} else {
			cout << i << "0-" << i << "9: ";
		}
		for(int j = 0; j < resultList[i]; j++) {
			cout << "X";
		}
		cout << endl;
	}

	return 0;
}


/*
* Reads an entire file into the Vector<string> supplied by the user.
*/
void ReadTextFile(ifstream & infile, Vector<string> & lines) {
	while (true) {
		string line;
		getline(infile, line);
		if (infile.fail()) break;
		lines.add(line);
	}
}

/*
* Opens a text file whose name is entered by the user. If the file
* does not exist, the user is given additional chances to enter a
* valid file. The prompt string is used to tell the user what kind
* of file is required.
*/
void AskUserForInputFile(string prompt, ifstream & infile) {
	while (true) {
		cout << prompt;
		string filename = GetLine();
		infile.open(filename.c_str());
		if (!infile.fail()) break;
		cout << "Unable to open " << filename << endl;
		infile.clear();
	}
}
