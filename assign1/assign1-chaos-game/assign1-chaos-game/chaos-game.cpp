/*
 * Project: assign1-chaos-game
 * Created by CS106 C++ Assignment Wizard 0.1
 *
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * [TODO: Describe assignment]
 */


#include <iostream>
#include "graphics.h"
#include <iomanip>
#include <random.h>
#include "extgraph.h"

using namespace std;


const double CIRCLE_RADIUS = 0.03;
struct point {
	double x;
	double y;
};

void drawSmallCircle(double r);

int main ()
{
	Randomize();
	InitGraphics();
	point pA;
	point pB;
	point pC;
	WaitForMouseDown();
	pA.x = GetMouseX();
	pA.y = GetMouseY();
	WaitForMouseUp();
	WaitForMouseDown();
	pB.x = GetMouseX();
	pB.y = GetMouseY();
	WaitForMouseUp();
	WaitForMouseDown();
	pC.x = GetMouseX();
	pC.y = GetMouseY();
	WaitForMouseUp();
	
	cout << "successfully get three points. " << endl
		<< "A.x: "<< pA.x << " A.y:" << pA.y << endl
		<< "B.x: "<< pB.x << " B.y:" << pB.y << endl
		<< "C.x: "<< pC.x << " C.y:" << pC.y << endl;
	point currentP = {-1, -1};
	point lastP = {-1, -1};
	
	while(!MouseButtonIsDown()) {
		
		int vertexNum = RandomInteger(1,3);
		switch(vertexNum) {
			case 1: currentP = pA;break;
			case 2: currentP = pB;break;
			case 3: currentP = pC;break;
			default: Error("Not valid vertex number.");
		}
		
		
		MovePen(currentP.x, currentP.y);
		drawSmallCircle(CIRCLE_RADIUS);
		UpdateDisplay();
		if(lastP.x == -1 && lastP.y == -1) {
			lastP.x = currentP.x;
			lastP.y = currentP.y;
		} else {
			lastP.x = (lastP.x + currentP.x)/2.0;
			lastP.y = (lastP.y + currentP.y)/2.0;
			MovePen(lastP.x, lastP.y);
			drawSmallCircle(CIRCLE_RADIUS);
			UpdateDisplay();
		}
		
	}
	


	
		
	return 0;
}


void drawSmallCircle(double r) {
	StartFilledRegion(1);
	DrawArc(r, 0, 360);
	EndFilledRegion();
}
