/*
 * Project: assign1VotingMachines
 * Created by CS106 C++ Assignment Wizard 0.1
 *
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * [TODO: Describe assignment]
 */
#include <iostream>
#include <random.h>
#include <iomanip>
#include "simpio.h"

using namespace std;

const int NUM_TRIALS = 500;

int main ()
{
	Randomize();
	int numVoters = 0, votersA, votersB, numInvalid = 0;
	double votePercentA, votePercentB, percentDiff = 0, percentError;

	while(true) {
		cout << "Enter number of voters:" << endl;
		numVoters =	GetInteger();
		if(numVoters < 100) {
			cout << "number of voters must be larger than 100." << endl;
		} else {
			break;
		}
	}

	while(true) {
		cout << "Enter percentage spread between candidates: " << endl;
		percentDiff = GetReal();
		if(percentDiff < 0 || percentDiff > .1) {
			cout << "percentage difference must be within 0 to 0.1" << endl;
		} else {
			break;
		}
	}


	while(true) {
		cout << "Enter voting error percentage: " << endl;
		percentError = GetReal();
		if(percentError < 0 || percentError > 1) {
			cout << "error percentage: must be within 0 to 1.0" << endl;
		} else {
			break;
		}
	}

	votePercentA = 0.5 + percentDiff;
	votePercentB = 0.5 - percentDiff;
	votersA = numVoters * votePercentA;
	votersB = numVoters * votePercentB;
	char* votersAList = new char[votersA];
	char* votersBList = new char[votersB];
	
	for(int trials = 0; trials < NUM_TRIALS; trials++) {

		for(int i = 0; i < votersA;i++) {
			if(RandomChance(percentError)) {
				votersAList[i] = 'B';
			} else {
				votersAList[i] = 'A';
			}
		}

		for(int i = 0; i < votersB;i++) {
			if(RandomChance(percentError)) {
				votersBList[i] = 'A';
			} else {
				votersBList[i] = 'B';
			}
		}

		int counterA = 0, counterB = 0;

		for(int i = 0; i < votersA; i++) {
			if(votersAList[i] == 'A') {
				counterA++;
			} else if(votersAList[i] == 'B'){
				counterB++;
			} else {
				Error("invalid value.");
			}
		}

		for(int i = 0; i < votersB; i++) {
			if(votersBList[i] == 'A') {
				counterA++;
			} else if(votersBList[i] == 'B'){
				counterB++;
			} else {
				Error("invalid value.");
			}
		}

		if(counterA < counterB) {
			numInvalid++;
		}

	}
	cout << "Chance of an invalid election result after "<< NUM_TRIALS <<" trials = " << setprecision(4) <<double(numInvalid)*100/NUM_TRIALS << "%" << endl;
	delete[] votersAList;
	delete[] votersBList;

	return 0;
}
