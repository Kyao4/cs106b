/*
 * Project: assign1Soundex
 * Created by CS106 C++ Assignment Wizard 0.1
 *
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * [TODO: Describe assignment]
 */

#include <iostream>
#include <cctype>
#include "simpio.h"


int main ()
{
	while(true) {
		cout << "Enter surname (RETURN to quit):" << endl;
		string name = GetLine();
		if(name == "") {
			exit(0);
		}
		
		char* nameCode = new char[name.length()];
		int codeCount = 0;
		for(int i = 0; i < name.length(); i++) {
			name[i] = toupper(name[i]);
			if(i == 0) {
				nameCode[0] = name[codeCount++];
				continue;
			}
			switch(name[i]) {
				case 'A':;
				case 'E':;
				case 'I':;
				case 'O':;
				case 'U':;
				case 'H':;
				case 'W':;
				case 'Y':nameCode[codeCount++] = '0';break;
				case 'B':;
				case 'F':;
				case 'P':;
				case 'V':nameCode[codeCount++] = '1';break;;
				case 'C':;
				case 'G':;
				case 'J':;
				case 'K':;
				case 'Q':;
				case 'S':;
				case 'X':;
				case 'Z':nameCode[codeCount++] = '2';break;
				case 'D':;
				case 'T':nameCode[codeCount++] = '3';break;
				case 'M':;
				case 'N':nameCode[codeCount++] = '4';break;
				case 'L':nameCode[codeCount++] = '5';break;
				case 'R':nameCode[codeCount++] = '6';break;
				default: Error("do not have this character.");
			}

			if(nameCode[codeCount - 1] == nameCode[codeCount - 2]) {
				codeCount--;
				nameCode[codeCount] = -1;
			} else {
				nameCode[codeCount] = -1;
			}
		}


		//cout << "name code is: " << nameCode << endl;
		string nameCodeStr = nameCode;
		//cout << "name code is: " << nameCodeStr << endl;

		for(int i = 1; i <= codeCount + 1; i++) {
			if(nameCodeStr[i] == '0') {
				nameCodeStr = nameCodeStr.substr(0, i) + nameCodeStr.substr(i + 1);
			}
		}

		//cout << "name code is: " << nameCodeStr << endl;
		

		nameCodeStr = nameCodeStr.substr(0, 4);
		//cout << "name code is: " << nameCodeStr << endl;
		for(int i = 1; i < 4; i++) {
			if( nameCodeStr[i] != '1' &&  nameCodeStr[i] != '2' && 
				 nameCodeStr[i] != '3' &&  nameCodeStr[i] != '4' &&  nameCodeStr[i] != '5' && 
				  nameCodeStr[i] != '6' &&  nameCodeStr[i] != '7' &&  nameCodeStr[i] != '8' && 
				  nameCodeStr[i] != '9') {
				nameCodeStr[i] = '0';
			}
		}

		for(int i = 1; i < name.length(); i++) {
			name[i] = tolower(name[i]);
		}
		cout << "Soundex code for " << name << " is " << nameCodeStr << endl;
	
	}
	return 0;
}
