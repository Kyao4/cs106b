
	// If implemented using Vector data mamber, default memberwise copy 
	// works fine, but if implemented as linked list, (ie pointer data member)
	// copying would create unintended sharing.
	// It's fine is to disallow copying for all implementations as
	// a precaution
	DISALLOW_COPYING(PQueue)

  	/* This is the representation for the unsorted vector.
  	 * You will need to update this as you change representations. */
	/*
	vector:

	Vector<int> entries;

	*/
	/*
	list:
	
	struct Cell {
		int value;
		Cell *next;
	};
	Cell *head;*/

	/*
	chunklist:

	const static int MaxElemsPerBlock = 4;
	struct Cell {
		int entries[MaxElemsPerBlock]; 
		int count;
		Cell *next;
	};
	Cell *head;*/

	//heap
	const static int INIT_SIZE = 10;
	int* entries;
	int capacity;
	int count;
	void swap(int & a, int & b);
	void printHeap(int index);