
	// If implemented using Vector data mamber, default memberwise copy 
	// works fine, but if implemented as linked list, (ie pointer data member)
	// copying would create unintended sharing.
	// It's fine is to disallow copying for all implementations as
	// a precaution
	DISALLOW_COPYING(PQueue)

  	/* This is the representation for the unsorted vector.
  	 * You will need to update this as you change representations. */
	Vector<int> entries;


	//struct Cell {
	//	int value;
	//	Cell *next;
	//};
	//Cell *head;