/*
 * Project: assign5B
 * Created by CS106 C++ Assignment Wizard 0.1
 *
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * [TODO: Describe assignment]
 */


#include <iostream>
#include "vector.h"
#include "random.h"
#include "genlib.h"
#include "cmpfn.h"
#include "set.h"


template <typename Type>
	void GnomeSort(Vector<Type> & numList, int (cmpFn)(Type, Type) = OperatorCmp);


int compareSetInt(Set<int> one, Set<int> two);
int compareString(string one, string two);
int main ()
{
	Vector<int> intList;
	for(int i = 0; i < 10; i++) {
		intList.add(RandomInteger(1, 50));
	}
	for(int i = 0; i < 10; i++) {
		cout << intList[i] << endl;
	}
	
	GnomeSort(intList);

	for(int i = 0; i < intList.size(); i++) {
		cout << intList[i] << endl;
	}


	Vector<string> strList;
	strList.add("test");
	strList.add("write");
	strList.add("line-for-line");
	strList.add("algorithm");
	strList.add("implement");
	strList.add("we");
	strList.add("breaking");
	strList.add("news");

	GnomeSort(strList, compareString);

	for(int i = 0; i < strList.size(); i++) {
		cout << strList[i] << endl;
	}
	Vector<Set<int>> setList; 
	Set<int> one;
	one.add(1);one.add(6);one.add(10);
	Set<int> two;
	two.add(3);two.add(5);
	Set<int> three;
	three.add(3);
	Set<int> four;
	four.add(5);four.add(9);four.add(10);four.add(11);
	Set<int> five;
	five.add(5);five.add(11);
	setList.add(one);setList.add(two);setList.add(three);setList.add(four);setList.add(five);

	GnomeSort(setList, compareSetInt);
	
	Set<int> currentSet;
	for(int i = 0; i < setList.size(); i++) {
		currentSet = setList[i];
		Set<int>::Iterator iter = currentSet.iterator();
		while(iter.hasNext()) {
			cout << iter.next() << " ";
		}
		cout << endl;
	}

	return 0;
}


template <typename Type>
	void GnomeSort(Vector<Type> & numList, int (cmpFn)(Type, Type)) {
	int pos = 1;
	while(pos <= numList.size() - 1) {
		if(pos == 0) {
			pos = 1;
		}
		if(cmpFn(numList[pos], numList[pos - 1]) > 0 || cmpFn(numList[pos], numList[pos - 1]) == 0) {
			pos += 1;
		} else {
			Type temp = numList[pos - 1];
			numList[pos - 1] = numList[pos];
			numList[pos] = temp;
			pos -= 1;
			
		}
	}
}


int compareString(string one, string two) {
	if(one.length() > two.length()) {
		return 1;
	} else if(one.length() == two.length()) {
		return 0;
	} else {
		return -1;
	}
}

int compareSetInt(Set<int> one, Set<int> two) {
	if(one.size() > two.size()) {
		return 1;
	} else if(one.size() == two.size()) {
		return 0;
	} else { 
		return -1;
	}
}