/*
 * Project: assign5A
 * Created by CS106 C++ Assignment Wizard 0.1
 *
 * Name: [TODO: enter name here]
 * Section: [TODO: enter section leader here]
 * [TODO: Describe assignment]
 */

/*
	Bubble sort n2
	Insertion sort n2
	//Merge sort nlogn
	//Quicksort nlogn
	Selection sort n2

	According to statisitcs on the testbook, quicksort is faster than Merge sort when input become really large
	So 2 is quicksort 1 is merge sort.

	if input is totally in descending order.
	the time that increase the most is 5, so it is bubble sort

*/


#include <iostream>
#include <ctime>
#include <iomanip>
#include "mysterysort.h"
#include "random.h"
#include "vector.h"

static int REPEAT_TIME = 100000;

int main ()
{
	//Randomize();
	Vector<int> input1, input, inputDescending;
	for(int i = 0; i < 10; i++) {
		input.add(RandomInteger(1, 50));
	}
	for(int i = 0; i < input.size(); i++) {
		cout << input[i] << endl;
	}
	for(int i = 10; i >= 1; i-- ) {
		inputDescending.add(i);
	}
	

	double start, end, elapsed;
	
	
	cout << "sort1 starts..." << endl;
	start = double(clock());
	for(int i = 0; i < REPEAT_TIME; i++) {
		input1 = input;
		MysterySort1(input1, 0.00000000000001);
		for(int i = 0; i < input1.size(); i++) {
			cout << input1[i] << endl;
		}
		break;
	}
	end = double(clock());
	elapsed = end - start;
	cout << "sort1 ends, elapse time: " << setprecision(3) << elapsed << endl;

	cout << "sort2 starts..." << endl;
	start = double(clock());
	for(int i = 0; i < REPEAT_TIME; i++) {
		input1 = input;
		MysterySort2(input1);
	}
	end = double(clock());
	elapsed = end - start;
	cout << "sort2 ends, elapse time: " << setprecision(3) << elapsed << endl;

	cout << "sort3 starts..." << endl;
	start = double(clock());
	for(int i = 0; i < REPEAT_TIME; i++) {
		input1 = input;
		MysterySort3(input1);
	}
	end = double(clock());
	elapsed = end - start;
	cout << "sort3 ends, elapse time: " << setprecision(3) << elapsed << endl;
	
	cout << "sort4 starts..." << endl;
	start = double(clock());
	for(int i = 0; i < REPEAT_TIME; i++) {
		input1 = input;
		MysterySort4(input1);
	}
	end = double(clock());
	elapsed = end - start;
	cout << "sort4 ends, elapse time: " << setprecision(3) << elapsed << endl;
	
	cout << "sort5 starts..." << endl;
	start = double(clock());
	for(int i = 0; i < REPEAT_TIME; i++) {
		input1 = input;
		MysterySort5(input1);
	}
	end = double(clock());
	elapsed = end - start;
	cout << "sort5 ends, elapse time: " << setprecision(3) << elapsed << endl;
	
	return 0;
}


